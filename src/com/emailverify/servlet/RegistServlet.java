package com.emailverify.servlet;

import com.emailverify.domain.User;
import com.emailverify.service.UserService;
import com.emailverify.service.impl.UserServiceImpl;
import com.emailverify.utils.UUIDUtils;

import java.io.IOException;
import java.sql.SQLException;

/**
 * @author shaoyifei
 * 用于注册的servlet
 * */
public class RegistServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        //可以用Map接受，这里简单点用String的parameter接受
        //处理中文乱码
        request.setCharacterEncoding("UTF-8");
        //接受数据
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String nickname = request.getParameter("nickname");
        String email = request.getParameter("email");

        //封装数据
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setNickname(nickname);
        user.setEmail(email);
        user.setState(0);//0:未激活    1:已经激活
        //验证码的生成是随机的，借用工具类生成64位的验证码
        String code = UUIDUtils.getUUID()+UUIDUtils.getUUID();
        user.setCode(code);

        System.out.println(user.toString());
        //调用业务层处理数据
        UserService userService = new UserServiceImpl();
        try {
            userService.regist(user);
        } catch (Exception e) {
            e.printStackTrace();
        }


        //页面跳转
        request.setAttribute("msg","您已注册成功，请去邮箱激活");
        request.getRequestDispatcher("/msg.jsp").forward(request,response);
    }
}
