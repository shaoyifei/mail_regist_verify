package com.emailverify.servlet;
/**
 * 用户激活的servlet
 * */
import com.emailverify.domain.User;
import com.emailverify.service.UserService;
import com.emailverify.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class ActiveServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //接受激活码
        String code  = request.getParameter("code");

        //根据激活码查询用户，需要在service层以及dao层增加findByCode方法
        UserService userService = new UserServiceImpl();
        try {
            User user = userService.findByCode(code);
            //判断查询的用户是否为空
            if(user!=null){
                //已经查询到，修改用户的状态，需要增加一个update方法
                user.setState(1);
                user.setCode(null);
                userService.update(user);
                request.setAttribute("msg","您已经激活成功！请登录");
            }else {
                //未查找到
                request.setAttribute("msg","您的激活码有误！请重新激活");
            }
            //页面跳转
            request.getRequestDispatcher("/msg.jsp").forward(request,response);
        } catch (SQLException e) {
            e.printStackTrace();
        }




    }
}
