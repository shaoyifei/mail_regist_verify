package com.emailverify.utils;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.QueryRunner;


/**
 * 工具类 1,初始化c3p0连接池 2,创建DbUtils核心工具类对象
 *
 * @author bighuan
 *
 */
public class JDBCUtils {

    /**
     * 1,初始化c3p0连接池
     */
    private static DataSource dataSource;

    static {
        dataSource = new ComboPooledDataSource();
    }

    /**
     * 创建DbUtils核心工具类对象
     *
     * @return
     */
    public static QueryRunner getQueryRunner() {
        /**
         * 创建QueryRunner对象,传入连接池对象(数据源)
         * 注意:在创建QueryRunner对象时,如果传入了数据源对象,那么在使用QueryRunner对象
         * 方法的时候,就不需要传入连接连接对象,会自动从数据源中获取连接(不需要关闭,会自动关闭)
         */
        return new QueryRunner(dataSource);
    }
}
