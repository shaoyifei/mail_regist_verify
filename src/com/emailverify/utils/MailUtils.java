package com.emailverify.utils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @author shaoyifei
 * 邮件发送工具类
 * */
public class MailUtils {
    /**
     * 发送邮件的方法
     * @param to    ：发邮件
     * @param code  ：激活码
     * */
    public static void sendMail(String to, String code) throws MessagingException {
        //1、创建连接对象，连接到邮箱服务器
        Properties props = new Properties();
        //可以设置服务器地址
        //qq邮箱的话host：smtp.qq.com
        //port：465
        props.setProperty("host","value");
        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                //通过邮箱的用户名密码，用于向用户发送邮件
                // qq邮箱需要设置用户名及授权码
                return new PasswordAuthentication("","");
            }
        });
        //2、创建邮件对象
        Message message = new MimeMessage(session);

        //2.1 设置发件人,发件人地址
        message.setFrom(new InternetAddress(""));

        //2.2 设置收件人
        message.setRecipient(Message.RecipientType.TO,new InternetAddress(to));

        //2.3 设置邮件主题
        message.setSubject("来自**网站的激活邮件");

        //2.4 设置邮件正文
        message.setContent("<h1>来自**网站的激活邮件，激活请点击连接：</h1><h3><a href='http://localhost:8080/email/regist_web/ActiveServlet?code="+code+"'>http://localhost:8080/regist_web/ActiveServlet?code="+code+"</a></h3>","text/html;charset=UTF-8");

        //3、发送一封激活邮件
        Transport.send(message);
    }
}
