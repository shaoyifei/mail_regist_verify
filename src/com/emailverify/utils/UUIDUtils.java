package com.emailverify.utils;

import java.util.UUID;

/**
 * 生成随机字符创的工具类
 * @author shaoyifei
 * */
public class UUIDUtils {
    public static String getUUID(){
        //借用util库中的方法生成随机32位字符串，字符串会有横杠
        return UUID.randomUUID().toString().replace("-","");
    }
}
