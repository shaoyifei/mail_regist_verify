package com.emailverify.service;

import com.emailverify.domain.User;

import java.sql.SQLException;

public interface UserService {
    /**
     * @param user  实体类对象
     * @author shaoyifei
     * */
    void regist(User user) throws Exception;

    User findByCode(String code)throws SQLException;

    void update(User user)throws SQLException;
}
