package com.emailverify.service.impl;

import com.emailverify.dao.UserDao;
import com.emailverify.dao.impl.UserDaoImpl;
import com.emailverify.domain.User;
import com.emailverify.service.UserService;
import com.emailverify.utils.MailUtils;

import java.sql.SQLException;

/**
 * 业务层处理逻辑
 * */
public class UserServiceImpl implements UserService {
    @Override
    /**业务层用户注册的方法
     * @author shaoyifei
     */
    public void regist(User user) throws Exception {
        //将数据存入数据库，需要用到数据库的操作类UserDao
        UserDao userDao = new UserDaoImpl();
        userDao.regist(user);

        //发送一份激活邮件
        MailUtils.sendMail(user.getEmail(),user.getCode());

    }

    @Override
    //根据激活码查询用户
    public User findByCode(String code) throws SQLException{
        UserDao userDao = new UserDaoImpl();
        User user = userDao.findByCode(code);
        return user;
    }

    @Override
    //更新数据库
    public void update(User user) throws SQLException {
        UserDao userDao = new UserDaoImpl();
        userDao.update(user);
    }
}
