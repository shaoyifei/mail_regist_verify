package com.emailverify.dao.impl;

import com.emailverify.dao.UserDao;
import com.emailverify.domain.User;
import com.emailverify.utils.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.sql.SQLException;

public class UserDaoImpl implements UserDao {
    @Override
    //DAO中保存用户的方法
    public void regist(User user) throws SQLException {
        //在utils类中完成数据库连接池的初始化
        QueryRunner queryRunner = JDBCUtils.getQueryRunner();
        String sql = "insert into user(username, password, nickname, email, state, code) values(?,?,?,?,?,?);";
        Object[] params = { user.getUsername(),user.getPassword(),user.getNickname(),
                        user.getEmail(),user.getState(),user.getCode() };
        queryRunner.update(sql,params);
    }

    @Override
    //根据code查找用户
    public User findByCode(String code) throws SQLException {
        QueryRunner queryRunner = JDBCUtils.getQueryRunner();
        String sql = "select * from user where code = ?";
        User user = (User)queryRunner.query(code, sql, new BeanHandler(User.class));
        return user;
    }

    @Override
    //更新数据库
    public void update(User user) throws SQLException {
        QueryRunner queryRunner = JDBCUtils.getQueryRunner();
        String sql = "update user set username=?,password=?,nickname=?,email=?,state=?,code=? where uid=?";
        Object[] params = { user.getUsername(),user.getPassword(),user.getNickname(),
                user.getEmail(),user.getState(),user.getCode(),user.getUid() };
        queryRunner.update(sql,params);
    }
}
