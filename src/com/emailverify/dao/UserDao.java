package com.emailverify.dao;

import com.emailverify.domain.User;

import java.sql.SQLException;

public interface UserDao {
    void regist(User user) throws SQLException;

    User findByCode(String code)throws SQLException;

    void update(User user)throws SQLException;
}
