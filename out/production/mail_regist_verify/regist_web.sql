USE regist_web;
CREATE TABLE user
(
    uid int PRIMARY KEY AUTO_INCREMENT,
    username varchar(20) DEFAULT null ,
    password varchar(20) DEFAULT null ,
    nickname varchar(20) DEFAULT null ,
    email varchar(30) DEFAULT null ,
    state int(11) DEFAULT null ,
    code varchar(64) DEFAULT null
) charset=utf8;
