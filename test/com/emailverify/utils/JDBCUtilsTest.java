package com.emailverify.utils;

import com.emailverify.dao.UserDao;
import com.emailverify.dao.impl.UserDaoImpl;
import com.emailverify.domain.User;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class JDBCUtilsTest {
    @Test
    public void jdbcTest() throws SQLException {
        //ComboPooledDataSource dataSource = new ComboPooledDataSource();
        //Connection conn = dataSource.getConnection();
        String sql = "insert into user(username, password, nickname, email, state, code) value (?,?,?,?,?,?)";
        Object[] param = { "shao", "nihao", "nickname", "email@163.com", 1,"sahsiqyigxuah"};
      /*  String sql = "select * from user";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        ResultSet rs = pstmt.executeQuery();
        while(rs.next()){
            System.out.println(rs.getString(1));
        }*/

        QueryRunner queryRunner = JDBCUtils.getQueryRunner();
        queryRunner.update(sql,param);

    }
    @Test
    public void registTest(){
        User user = new User();
        user.setUsername("shaoyi");
        user.setPassword("user");
        user.setNickname("nihao");
        user.setEmail("email@.com");
        user.setState(0);
        user.setCode("sjoiqhsu");

        UserDao dao = new UserDaoImpl();
        try {
            dao.regist(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
